<?php


namespace App\Models;

use App\Core\Database;


class ArticleModel extends Database
{

    /**
     * Gets all article from BDD
     * 
     * @return array result of the query
     */
    public function getAllArticles()
    {
        $req = 'SELECT * FROM articles';
        return $this->getDB()->query($req)->fetchAll();
    }
/**
 * 
 *
 * @param integer $id
 * @return array article
 */
    public function getArticleById(int $id) {
        $sth = $this->getDB()->prepare('SELECT * FROM articles WHERE id = :id');
        $sth->bindParam(':id', $id);
        return $sth->fetchAll();
    }
/**
 * Undocumented function
 *
 * @param string $name
 * @return array article
 */
    public function getArticleByName(string $name) {
        $sth = $this->getDB()->prepare('SELECT * FROM articles WHERE title = :article_name');
        $sth->bindParam(':article_name', $name);
        return $sth->fetchAll();
    }
/**
 * Undocumented function
 *
 * @return boolean from db query
 */
    public function createArticle() {
        $stmt = $this->getDB()->prepare('INSERT INTO articles VALUES (null, :title, :content, :userid)');
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':content', $content);
        $stmt->bindParam(':userid', $userId);
        return $stmt->execute();
    }
/**
 * Undocumented function
 *
 * @param integer $id
 * @param string $title
 * @param string $content
 * @return boolean from db query
 * 
 */
    public function updateArticle(int $id,string $title, string $content) {
        $stmt = $this->getDB()->prepare('UPDATE articles SET title = :title, content = :content WHERE id = :article_id');
        $stmt->bindParam(':article_id', $id);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':content', $content);
        return $stmt->execute();
    }
/**
 * Undocumented function
 *
 * @param integer $id
 * @return boolean from db query
 */
    public function deleteArticle(int $id) {
        $stmt = $this->getDB()->prepare('DELETE FROM articles WHERE id = :id');
        $stmt->bindParam(':id', $id);
        return $stmt->execute();
    }
}
