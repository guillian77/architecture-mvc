<?php


namespace App\Controller;

use App\Models\ArticleModel;


class ArticleController extends MainController
{

    /**
     * Renders Views/Article/indexArticle
     */
    public function index()
    {
        require '../src/Models/ArticleModel.php';
        $myModel = new ArticleModel();
        $articles = $myModel->getAllArticles();
        $this->render("../src/Views/Article/indexArticle.php", $articles);
    }

    public function getAllApi() {
        hlhfglzhg;
    }

    /**
     * Renders Views/Article/createArticle
     */
    public function create()
    {
        $this->render("../src/Views/Article/createArticle.php");
    }

    /**
     * Renders Views/Article/editArticle
     */
    public function edit()
    {
        $article = ["title" => "mon titre", "content" => 'mon contenu'];
        $this->render("../src/Views/Article/editArticle.php", $article);
    }

    /**
     * Renders Views/Article/updateArticle
     */
    public function update()
    {
        dd('update');
    }

    /**
     * Renders Views/Article/deleteArticle
     */
    public function delete()
    {
        dd('delete');
    }
}
