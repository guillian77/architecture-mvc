<?php


namespace App\Controller;

use App\Core\Request;

class MainController
{
    public $request;

    public function __construct()
    {
        $this->request = new Request();
    }

    /**
     * Renders view.
     *
     * @param string $path
     * @param mixed $data
     * @return void
     */
    protected function render($path, $data = null)
    {
        require $path;
    }
}
