<?php
namespace App\Core;

class Autoload
{
    public $namespace;
    public $classname;

    public function __construct()
    {
        spl_autoload_register(function($className)
        {
            $exploded = explode("\\", $className);
            $this->classname = $exploded[count($exploded)-1];
            $this->namespace = trim( str_replace($exploded[count($exploded)-1], "", $className), "\\" );

            $this->load( $this->associate() );
        });
    }

    /**
     * Assign physical directory from namespace called.
     *
     * @return string
     */
    public function associate()
    {
        $dir = "";

        switch ($this->namespace) {
            case 'App\Core':
                $dir = "../src/Cores/" . $this->classname . ".php";
                break;
            case 'App\Controller':
                $dir = "../src/Controllers/" . $this->classname . ".php";
                break;
            case 'App\Models':
                $dir = "../src/Models/" . $this->classname . ".php";
                break;
        }

        return $dir;
    }

    /**
     * Load class from classname.
     *
     * @param $dir
     */
    public function load($dir)
    {
        if (file_exists($dir)) {
            require $dir;
        } else {
            http_response_code(404);
        }
    }
}