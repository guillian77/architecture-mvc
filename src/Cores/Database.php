<?php

namespace App\Core;

class Database {

    private $handle;

    public function __construct() {    
       $myConfig =  (object) Configuration::database();
       $this->handle = new \PDO("mysql:host=" . $myConfig->hostname . ";dbname=" . $myConfig->database, $myConfig->username, $myConfig->pwd);
    }

    public function getDB() {
       return $this->handle; 
    }

}