<?php

namespace App\Core;


class Dispatcher
{
    public function __construct()
    {
        $request = new Request();
        $className = "\App\Controller\\" . ucfirst($request->controller) . "Controller";

        if (class_exists($className)) {
            ob_start();
            $classLoaded = new $className();
            call_user_func([$classLoaded, $request->action], []);
            $content = ob_get_clean();
            require '../public/layout.php';
        } else {
            http_response_code(404);
        }
    }
}
