<?php

namespace App\Core;

class Configuration {

    /**
     * Return Database configuration
     * @return array
     */
    public static function database() {
        return [
        "database"=>"mvc-architecture",
        "hostname"=>"",
        "username"=>"root",
        "pwd"=>""
        ];
    }
}