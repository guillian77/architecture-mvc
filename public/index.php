<?php

use \App\Core\Autoload;
use \App\Core\Dispatcher;

function dd($toDebug)
{
    echo "<pre>";
    print_r($toDebug);
    echo "</pre>";
}



require '../src/Cores/Autoload.php';
require '../src/Cores/Dispatcher.php';
require '../src/Cores/Database.php';


new Autoload();
new Dispatcher();
